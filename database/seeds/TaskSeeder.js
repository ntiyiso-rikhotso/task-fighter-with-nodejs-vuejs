'use strict';

/*
|--------------------------------------------------------------------------
| TaskSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory');
const Database = use('Database');

class TaskSeeder {
  async run() {
    await Database.table('tasks').insert([
      {name : 'Get Older', priority: 50, dueIn : 365},
      {name : 'Breathe', priority: 1000, dueIn : 30},
      {name : 'Complete Assessment', priority: 50, dueIn : 15},
    ])
  }
}

module.exports = TaskSeeder;
