'use strict';


const TaskTickService = use('App/Services/TaskTickService');
class TaskFighter {


  constructor(name, priority, dueIn) {
    this.name = name || null;
    this.priority = priority || null;
    this.dueIn = dueIn || null;
  }

  static of(name, priority, dueIn) {
    return new TaskFighter(name, priority, dueIn);
  }

  tick() {
    let taskName = this.name.toLowerCase();
    let taskFighter = TaskTickService.getService(taskName);

    this.priority = taskFighter.priority;
    this.dueIn = taskFighter.dueIn;


  }

}

module.exports = TaskFighter;
