class TickServiceAbstract {


  constructor(taskFighter) {
    if (new.target === TickServiceAbstract) {
      throw new TypeError('Cannot construct Abstract instances directly');
    }

    if (typeof this.tick === 'undefined') {
      throw new TypeError('Missing method `tick()`');
    }

    this.taskFighter = taskFighter;
  }

  tick() {
    console.error('Please call super() in the construct class');
  }

  response() {
    return this.taskFighter;
  }

}

module.exports = TickServiceAbstract;

