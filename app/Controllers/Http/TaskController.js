'use strict';

const Task = use('App/Models/Task');
const TaskFighter = use('App/TaskFighter');

class TaskController {

  home({view}) {
    return view.render('home');
  }

  async tick() {
    let tasks = await Task.all();

    for (let task in tasks) {
      if(typeof tasks === 'object'){
        let taskFighter = TaskFighter.of(task.name, task.priority, task.dueIn);
        taskFighter.tick();
        task.update({
          priority: 0,
          dueIn: 0,
        });
      }

    }
  }

  async index({request, response, view}) {
    return response.json((await Task.all()));
  }

  async create({request, response, view}) {
  }

  async store({request, response}) {
  }

  async show({params, request, response, view}) {
    return response.json(await Task.find(params.id));
  }

  async edit({params, request, response, view}) {
  }

  async update({params, request, response}) {
  }

  async destroy({params, request, response}) {
  }
}

module.exports = TaskController
