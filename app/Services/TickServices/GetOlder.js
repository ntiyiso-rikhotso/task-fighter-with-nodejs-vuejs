const TickServiceAbstract = use('App/Abstracts/TickServiceAbstract');
class GetOlder extends TickServiceAbstract {
  constructor(taskFighter) {
    super(taskFighter);
  }

  tick() {
    --this.taskFighter.dueIn;
    if (this.taskFighter.priority > 0) {
      --this.taskFighter.priority;
      if (this.taskFighter.dueIn < 0) {
        --this.taskFighter.priority;
      }
    }

    return this.taskFighter;
  }
}

module.exports = GetOlder
