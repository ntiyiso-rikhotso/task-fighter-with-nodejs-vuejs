const TickServiceAbstract = use('App/Abstracts/TickServiceAbstract');
class Default extends TickServiceAbstract {
  constructor(taskFighter) {
    super(taskFighter);
  }

  tick() {
    --this.taskFighter.dueIn;
    if (this.taskFighter.priority < 100) {
      ++this.taskFighter.priority;
    }

    return this.taskFighter;
  }
}

module.exports = Default
