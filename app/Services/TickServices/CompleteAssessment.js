const TickServiceAbstract = use('App/Abstracts/TickServiceAbstract');

class CompleteAssessment extends TickServiceAbstract{
  constructor(taskFighter) {
    super(taskFighter);
  }
  tick() {
    --this.taskFighter.dueIn;
    if (this.taskFighter.priority < 100) {
      ++this.taskFighter.priority;
      if (this.taskFighter.dueIn < 11) {
        ++this.taskFighter.priority;
      }
      if (this.taskFighter.dueIn < 6) {
        ++this.taskFighter.priority;
      }
    }

    return this.taskFighter;
  }
}

module.exports = CompleteAssessment;
