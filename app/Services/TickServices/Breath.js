const TickServiceAbstract = use('App/Abstracts/TickServiceAbstract');

class Breathe extends TickServiceAbstract{
  constructor(taskFighter) {
    super(taskFighter);
  }
  tick() {
    if(this.taskFighter.dueIn < 0){
      this.taskFighter.priority -= this.taskFighter.priority;
    }

    return this.taskFighter;
  }
}

module.exports = Breathe;
