const GetOlder  = use('App/Services/TickServices/GetOlder');
const Breathe = use('App/Services/TickServices/Breath');
const CompleteAssessment = use('App/Services/TickServices/CompleteAssessment');
const Default = use('App/Services/TickServices/Default');
const ALIASES = {
  'get older' : GetOlder,
  'breathe' : Breathe,
  'complete assessment' : CompleteAssessment,
};
class TaskTickService{

  static getService(taskName, task) {
       let service = ALIASES[taskName] || Default;

    return (new service(task)).tick()
  }
}

module.exports = TaskTickService;
