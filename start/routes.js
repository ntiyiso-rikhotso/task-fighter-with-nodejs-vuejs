'use strict';

const Route = use('Route');

Route.resource('api/task', 'TaskController').apiOnly();
Route.get('/', 'TaskController.home');
Route.get('/list/tick', 'TaskController.tick');
